/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/counter.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import Vue from 'vue';

var jsapp=new Vue({
    el:"#counter",
    delimiters:['{*','*}'],
    data:{
        number:0,  
    },
    methods: {
        add() {
          this.number = this.number+1;
        }
    }
});


