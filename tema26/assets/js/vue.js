/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/vue.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');
require('bootstrap');
import axios from 'axios';
import Vue from 'vue';

var jsapp=new Vue({
    el:"#js-app",
    delimiters:['{*','*}'],
    data:{
        x:0,
        y:0,
        message:"hello",
        value:"",
        json: null,
        note: [ {simona:'6'},
                {simona:'7'}
        

        ],
        noteObj: [ {
            firstName: 'Paul',
            lastName: 'Barna',
            nota: 8
        },
        { firstName: 'Flaviu',
          lastName: 'Turcut',
          nota: 7

        }

        ],
        title:"title",
        st: true
    },
    methods:{
        alertMe: function(){
            alert(this.value);
         },
         mPosition: function(event){
              this.x= event.clientX;
              this.y=event.clientY;
            //   console.log(event);
        },
        changeText: function(){
            this.message= "hello again";
            this.title="new title"
        },
        changeTitle: function(){
            if (st)
            st=false;
            else st=true;
        }
    },
    mounted: function(){
        axios
        .get('http://curs22-env.local/json')
        .then(response=>(this.json=response))
    }
});


