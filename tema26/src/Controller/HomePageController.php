<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index()
    {
        $menuActionLinks= array("link"=>"http://action1", "title"=>"action1");

        return $this->render('home_page/index.html.twig', [
            'action_links' => $menuActionLinks,
            
        ]);
    }
}
