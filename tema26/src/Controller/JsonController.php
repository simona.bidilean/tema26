<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class JsonController extends AbstractController
{
    /**
     * @Route("/json", name="json")
     */
    public function index()
    {
       $response= new Response();
       $response -> setContent(json_encode([
           'note'=> [
               'nota1'=> 8,
               'nota2'=> 6,
               'nota3'=> 9
            // 8, 9, 7
           ],
       ]));
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }
}
